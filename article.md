---
title: "Základní příjem: Otázky a odpovědi"
perex: "Švýcaři o víkendu v referendu rozhodují o zavedení základního nepodmíněného příjmu. S možností, že by vláda místo komplikovaných sociálních dávek mohla platit každému občanovi fixní částku, koketuje i Kanada (na&nbsp;snímku premiér Justin Trudeau) nebo Finsko. Jaké jsou výsledky prvních experimentů?"
description: "Švýcaři o víkendu v referendu rozhodují o zavedení základního nepodmíněného příjmu. S možností, že by vláda místo komplikovaných sociálních dávek mohla platit každému občanovi fixní částku, koketuje i Kanada (na snímku premiér Justin Trudeau) nebo Finsko. Jaké jsou výsledky prvních experimentů?"
authors: ["Jan Boček", "Petr Kočí", "Marcel Šulek"]
published: "3. června 2016"
coverimg: https://interaktivni.rozhlas.cz/data/zakladni-prijem/www/media/trudeau.jpeg
url: "zakladni-prijem"
libraries: [jquery, highcharts,]
---

### O čem budou Švýcaři rozhodovat?

Každý dospělý Švýcar by podle iniciativy, která návrh referenda vyvolala, měsíčně dostal 2500 švýcarských franků (asi 61 tisíc korun, ale životní náklady jsou oproti Česku trojnásobné). Federální vláda i parlament jsou proti: podle nich by muselo dojít k výraznému zvýšení daní, ani s ním by ale základní příjem nemohl zcela nahradit současné sociální dávky. Jeden z hlavních argumentů pro základní příjem – zjednodušení nebo úplné zrušení systému sociálního zabezpečení – je tedy podle vlády nereálný.

Volbu zřejmě ovládnou odpůrci novinky, podle [průzkumu z poloviny dubna](http://www.basicincome.org/news/2016/04/swiss-poll-40-percent-favour-referendum/) je 43 % Švýcarů pro, 57 % proti. Průzkum zároveň ukazuje, jak by se rozhodovala každá část trojjazyčné země: ve francouzské části by základní příjem prošel, v italské a zejména německé nikoli.

### Co přesně pojem *nepodmíněný základní příjem* znamená?

Univerzální nebo nepodmíněný základní příjem je částka, kterou pravidelně dostává každý občan země nebo obyvatel území. Nárok

- mají všichni, není určen jen pro chudé nebo nemocné,
- je automatický, není potřeba aktivně hledat práci a
- pro všechny je částka stejně vysoká.

Základní příjem může klasický systém sociálního zabezpečení doplnit nebo docela nahradit. Existují proto dvě varianty základního příjmu: *částečný základní příjem* přibližně na úrovni minimální mzdy a *plný základní příjem*, který by měl uspokojit běžné potřeby i kulturní vyžití. Proto se obvykle pohybuje kolem mediánu skutečných příjmů v zemi.

Podobná myšlenka, která navíc umožňuje odstupňovat výši příspěvku podle skutečných příjmů, je negativní daň z příjmu. Jde o variantu běžné daně z příjmu, kdy by do určité výšky daňoví poplatníci dostávali od státu benefit. Vyšší příjem by podléhal obvyklému zdanění.

### Kde se vzal?

Poprvé přišel s nápadem na základní příjem filosof Thomas Paine na konci 18. století. V eseji [Zemědělské právo](https://www.ssa.gov/history/paine4.html) navrhuje vytvořit fond, který bude každému dospělému vyplácet deset liber ročně. Smysl poplatku měla být vážně míněná *„kompenzace za ztrátu přírodního dědictví, která vznikla zavedením možnosti vlastnit půdu.“*

Téma se znovu vynořilo až v šedesátých letech minulého století ve Spojených státech. Libertariánský (tedy pravicový) ekonom Milton Friedman horoval za negativní daň z příjmu (i radikálnější myšlenky, jako [shazování peněz z vrtulníku](https://www.weforum.org/agenda/2015/08/what-is-helicopter-money/)). Negativní daň si během čtyř experimentů vyzkoušela i řada amerických měst. Republikánský prezident Nixon dokonce málem prosadil plán na zavedení částečného základního příjmu. Sněmovnou reprezentantů jeho návrh prošel, skončil až v Senátu.

Dnes je toto téma bližší levici. Zájem o základní příjem u levicových stran pramení v nedávné dluhové krizi, která v zemích jižní Evropy prudce zvýšila nezaměstnanost. Nepřekvapí tedy, že právě tam je základní příjem nejpopulárnější. Ve Španělsku jej do svého programu zařadila nová socialistická strana Podemos, která v posledních parlamentních hlasech – rok po svém založení – získala pětinu křesel v poslanecké sněmovně.

Jedním z nejvášnivějších zastánců je bývalý řecký ministr financí Varufakis; v [březnovém rozhovoru pro The Economist](http://www.economist.com/ESDvaroufakis) zmiňuje základní příjem jako jednu z možností, jak oživit [skomírající sociálnědemokratické strany](http://www.economist.com/news/briefing/21695887-centre-left-sharp-decline-across-europe-rose-thou-art-sick?fsrc=scn/tw/te/pe/ed/rosethouartsick) napříč Evropou.

Jinou, ale stejně aktuální motivaci pro testování základního příjmu mají severské sociální státy. Nepodmíněný příjem pro ně představuje nástroj, jak zjednodušit komplikované a drahé systémy sociálního zabezpečení. Proto také Finové chystají rozsáhlý experiment.

Raketový start debaty v posledních letech potvrzuje Guy Standing, britský ekonom a jeden z hlavních obhájců nepodmíněného příjmu, který v osmdesátých letech založil [platformu pro základní příjem (BIEN)](http://www.basicincome.org/). Letos v květnu [představil svoje téma v pražském Doxu](http://www.dox.cz/cs/doprovodne-akce/o-penezich-a-lidech).

<aside class="big">
  <figure>
    <div class="highcharts" id="container" style="height: 700px"></div>
  </figure>
  <figcaption>
    Zdroj: <a href="http://stats.oecd.org/Index.aspx?datasetcode=SOCX_AGG">OECD 2011</a>
  </figcaption>
</aside>

### Co základní příjem řeší a co mu kritici vyčítají?

> Přibývá chudých a roste nezaměstnanost

Jak ukázal rok 2008 a roky následující, otřesy globální ekonomiky můžou být kritické pro ekonomiky lokální. Podobné události (takzvané <a href="https://en.wikipedia.org/wiki/The_Black_Swan_(Taleb_book)">černé labutě</a>) jsou náhodné a hrozí neustále, lze je předvídat jen obtížně nebo vůbec. Negativní dopad na ekonomiku regionu i množství pracovních míst je přitom značný.

Pracovní místa mizí také z lépe předvídatelných, ale stejně neodvratných příčin: přesunem výroby za levnější pracovní silou, zánikem těžkého a těžařského průmyslu nebo kvůli/díky automatizaci.

Zastánci základního příjmu v něm vidí řešení zmíněných problémů. Jednak proto, že by zmírnil negativní dopady vysoké nezaměstnanosti, včetně posilování populistických a radikálních stran. Zároveň podle nich dává prostor k reformě pracovního trhu; pro nezaměstnané by bylo snazší rekvalifikovat se nebo inovovat a vytvářet nová pracovní místa.

Odpůrci naopak tvrdí, že základní příjem je *„opium pro nekvalifikované masy“*. Na propady a změny na pracovním trhu je podle nich třeba reagovat – stejně jako kdykoliv v minulosti, kdy lidstvu zkomplikovala život nová technologie – aktivním hledáním nových příležitostí. Polehávání na gauči se zaručeným příjem je podle nich příjemné, ale nefunkční řešení.

Dosavadní experimenty se základním příjmem ovšem stojí spíše na straně jeho zastánců. Během pětiletého pokusu v kanadském Dauphinu se u adresátů příjmu počet odpracovaných hodin snížil jen nepatrně, v jednotkách procent.

Nevýhodou podobných pokusů je fakt, že jsou ohraničené v čase a příjemci peněz vědí, že o ně v dohledné době přijdou, což může jejich chování ovlivňovat. Jak velký efekt má nepodmíněný příjem na chuť pracovat, tedy se stoprocentní jistotou zjistíme až po jeho uvedení v život.

> Současné systémy sociálního zabezpečení jsou příliš složité a byrokratické

Po druhé světové válce zapustily v Evropě kořeny systémy sociálního zabezpečení, které mají být mimo jiné pojistkou proti návratu totalitních režimů. Od té doby ovšem řada z nich rozkošatěla a často nedokážou reagovat na současné výzvy (ani na ty sedmdesát let staré).

S tím se vážou dva problémy: za prvé, současné sociální zabezpečení má vysoké náklady na administraci a armáda úředníků obvykle dál roste. Druhou chybou je neefektivita opatření, která mají motivovat k práci. Takzvaná *„past sociálního zabezpečení“ (welfare trap)* popisuje stav, kdy se práce ekonomicky nevyplatí, protože rostoucí výdělek nedokáže pokrýt výpadek některé sociální dávky.

Právě základní příjem by podle jeho zastánců mohl zastaralé systémy nahradit nebo doplnit tak, aby obě překážky zmizely. Automatický příjem v pevné výši by znamenal konec propočítávání, kdy se vyplatí pracovat, a kdy už ne. Úředníci by přišli o korumpující moc rozhodovat o sociálních výhodách, svět by přišel o část úředníků. Méně byrokratů by zároveň znamenalo víc peněz do rozpočtu.

Odpůrci namítají, že takhle růžové to není. Současné systémy sociálního zabezpečení mají poměrně značné výchylky, a to z dobrého důvodu: zdravotně postižení mohou dostávat výrazně nadprůměrné příspěvky, příspěvek na bydlení se může lišit podle cenové hladiny v regionu a podobně. Podle kritiků by základní příjem musel tyto nerovnosti kopírovat a ztratil by tak kouzlo jednoduchosti.

> Základní příjem by povzbudil stagnující ekonomiky

Obhájci základního příjmu v duchu keynesiánské tradice věří, že pravidelný příliv peněz by dokázal povzbudit unavené ekonomiky, chycené do pasti nízké inflace nebo deflace. Zdravá ekonomika obvykle vykazuje jistou míru inflace, která pomáhá znehodnocovat minulé dluhy a povzbuzuje k investicím. Naopak deflace dluhy prohlubuje. Například japonskou ekonomiku přitom deflace drží při zemi už dvacet let.

Při nedávné ekonomické krizi stimulovaly národní banky ekonomiku takzvaným *kvantitativním uvolňováním*, jehož cílem je zvýšit množství peněz v oběhu. Nebyly příliš úspěšné. Libertariánský ekonom Milton Friedman v šedesátých letech navrhoval spravedlivější vyhazování peněz z vrtulníku. Současná levice věří na základní příjem. Peníze pro každého by podle ní zvedla spotřebu a dala ekonomice potřebnou injekci.

Ekonom Lukáš Kovanda z finanční skupiny Roklen to vidí jinak. *„Základní příjem by se ve většině zemí neobešel bez citelného zvýšení daní. I kdyby dodatečná daňová zátěž zasahovala převážně bohaté lidi s nízkým sklonem ke spotřebě (bohatí spotřebovávají menší část svého příjmu), daňové navýšení je tak citelné, že by vedlo k útlumu spotřeby. Proti sobě by tedy stály dva efekty: zvýšení spotřeby díky vyššímu příjmu a snížení spotřeby kvůli vyšším daním.“*

*„Navíc není jisté, zda by vyšší příjem posílil spotřebu dlouhodobě. Je taky možné, že impulz by byl jen krátkodobý: lidé by si na více peněz rychle zvykli a začali by spořit,“* dodává Kovanda.</p>


> S prázdným žaludkem není dobré chodit k volbám

Zastánci nepodmíněného příjmu tvrdí, že politická svoboda není úplná, pokud ji nedoprovází ekonomická svoboda. Voliče, kteří počítají každou korunu, je snadnější zmanipulovat než ty, kteří mají čas a zájem aktivně vyhledávat informace.

Ekonom [Guy Standing](https://www.theguardian.com/business/economics-blog/2014/dec/18/incomes-scheme-transforms-lives-poor) popsal svoje zkušenosti z Indie, kde základní příjem zajišťoval UNICEF. Část chudých rodin se díky tomu vyvázala z dluhových pastí s vysokými úroky, další se dokázaly vypořádat s nečekanými událostmi. Základní příjem u nich vedl k emancipaci a nově nabytému sebevědomí.

> Peníze pro každého by znamenaly šťastnější společnost

Nepodmíněný příjem znamená podle svých obhájců šťastnější společnost. Takzvaná *ekonomie štěstí* přitom ukazuje, že tak jednoduché to není. Ke štěstí vede pouze relativní bohatnutí, tedy bohatství v porovnání s okolím. Pokud skokově zbohatne celá populace, což je případ základního příjmu, ke štěstí to nevede; alespoň ne dlouhodobě.

[Studie dvou amerických psychologů](http://pages.ucsd.edu/~nchristenfeld/Happiness_Readings_files/Class 3 - Brickman 1978.pdf) o výhercích v loterii navíc ukazuje, že na peníze člověka rychle přestávají motivovat. Výherci multimilionové sumy uvádějí během několika měsíců srovnatelnou úroveň životní spokojenosti jako před výhrou.

K podobnému závěru došel také americký Národní úřad pro ekonomický výzkum. V [loni publikované studii](http://www.nber.org/papers/w21806) zkoumal efekt skokového zdvojnásobení učitelských platů v Indonésii. Po dvou až třech letech od zvýšení platů už nebyl pozorován vůbec žádný efekt na výkonnost kantorů. A nejen to: autoři studie neshledali kladný dopad ani na studijní výsledky studentů. Závěr autorů zní, že šlo o vyhozené peníze.

> Jenže základní příjem je příliš drahý

Varianta nepodmíněného příjmu, o které budou v neděli rozhodovat Švýcaři, [je podle britského The Economist neudržitelně drahá](http://www.economist.com/news/finance-and-economics/21651897-replacing-welfare-payments-basic-income-all-alluring). Základní příjem těsně nad hranicí chudoby, který navrhují, by podle Britů spolkl 30 procent národního HDP. I při zrušení současného systému sociálního zabezpečení – což ale švýcarská vláda odmítá – by to znamenalo zvýšení daní o desítky procent nebo enormní šetření na veřejných výdajích. Dnešní daňové zatížení v zemi je mimochodem 44 procent, mezi zeměmi OECD jedno z nejvyšších.

*„Pokud bychom chtěli univerzální příjem na hranici chudoby (10 220 Kč měsíčně), musel by se objem vybraných daní zvýšit zhruba o třetinu,“* odhaduje ekonom Kovanda podmínky pro zavedení základního příjmu v Česku. *„Pokud by nám stačil základní příjem na úrovni životního minima (3400 Kč měsíčně), daňové navýšení by už nemuselo být tak citelné. V takové podobě by ale základní příjem příliš neměl smysl. Řada opravdu potřebných lidí je totiž na tom lépe dnes – při existenci cílených sociálních dávek – než by byla při takto nízké úrovni základního příjmu.“*

*„Dnes je u nás celkové zdanění kolem 35 procent. Financovat základní příjem z daní mi přijde nereálné. Jediným řešením je objevit ropu a základní příjem platit z její prodeje,“* hledá řešení Kovanda.


Zastánci základního příjmu ovšem tvrdí, že část peněz lze získat zrušením současných systémů sociálního zabezpečení a propuštěním úředníků. Kromě toho jsou přesvědčeni, že základní příjem a příležitosti, které otevírá, by znamenaly rychlý rozvoj ekonomiky.

> Základní příjem by posílil nežádoucí imigraci

Odpůrci zdůrazňují, že země se základním příjmem by ještě víc přitahovaly imigranty ze zemí třetího světa. Zastánci konceptu odpovídají celkem logicky, že je tedy třeba zavést základní příjem u nich doma. Podle nich tím naopak lze nelegální imigraci oslabit.

### Co víme z dosavadních experimentů?

První vážně míněné experimenty proběhly ve Spojených státech na konci šedesátých a v sedmdesátých letech. Atmosféra doby tématu nahrávala: v roce 1964 vyhlásil prezident Lyndon Johnson válku chudobě a za mořem se rozbíhal jeden sociální program za druhým. Během čtyř velkých experimentů v New Jersey, Denveru nebo Seattlu americká administrativa otestovala negativní daň z příjmu. Na konci sedmdesátých let se ovšem pokusy bezvýsledně rozplynuly – také proto, že předběžná vědecká zpráva odhalila u adresátů základního příjmu víc než padesátiprocentní zvýšení rozvodovosti. Strach z příliš samostatných žen konzervativcům pomohl téma potopit; až v devadesátých letech se ukázalo, že ke zvýšení rozvodovosti ve skutečnosti během experimentů nedošlo, jen kdosi udělal chybu ve výpočtu.

Nejrozsáhlejší pokus se základním příjmem se odehrál ve stejné době v Kanadě. Nepodmíněný příjem dostávali všichni obyvatelé osmitisícového farmářského městečka Dauphin v kanadské provincii Manitoba. Třetina nejchudších – pracujících i bez práce – měla navíc nárok na sociální příspěvek. Dauphinský experiment je mimořádný hlavně svou délkou, trval od roku 1974 do roku 1979.

Nepodmíněný příjem pro Dauphin zastřešil (a zaplatil) liberální premiér Kanady Pierre Trudeau. V červnu 1979 ovšem prohrál volby a nová konzervativní vláda experiment na hodinu ukončila. Výzkumníkům neumožnila výsledky experimentu vyhodnotit a sepsat závěrečnou zprávu. Originální data se naštěstí zachovala; výzkumníci dotazníky ze sociologického šetření naskládali do 1800 krabic a zapečetili. Výsledky, které máme, vycházejí z izolovaných studií na těchto datech, prováděných od osmdesátých let do dneška.

Hlavním cílem experimentu bylo odhalit, jaký vliv budou mít nepodmíněné peníze na chuť pracovat. Ukázalo se – možná překvapivě – že slabý. Podle [studie ekonomů z univerzity v Manitobě z roku 1993](https://login.uml.idm.oclc.org/login?url=http://www.jstor.org/stable/pdf/2535174.pdf?_=1463425254032) se počet odpracovaných hodin snížil u mužů (svobodných i ženatých) o 1 procento, u sezdaných žen o 3 procenta a u svobodných žen o 5 procent.

Na to navázala [studie stejného ústavu z roku 2011](http://public.econ.duke.edu/~erw/197/forget-cea%20(2.pdf)). Upřesnila, že zájem o práci se výrazně snížil u dvou skupin, čerstvých matek a školáků na prvním a druhém stupni. U obou z bohabojných důvodů, matky se více věnovaly dětem, školáci studiu. V Dauphinu coby farmářském městečku v indiánské rezervaci byli žáci zvyklí pomáhat rodičům s prací; po zavedení základního příjmu se podle výzkumníků více věnovali škole, což mimo jiné vedlo k lepším studijním výsledkům. Analýza také odhalila osmiprocentní pokles hospitalizací, nejvýraznější v oblasti psychiatrické péče.

Dauphinský experiment splňuje poměrně přísné vědecké nároky, má ovšem jednu samozřejmou chybu – je dočasný. Pětileté období nabízí značný vhled, ale účastníci pokusu věděli, že jednoho dne skončí a oni se budou opět muset bezezbytku spolehnout na farmaření a jinou poctivou práci. Proto je jeho výsledky třeba brát mírně s rezervou.

Dauphin nadlouho v Kanadě ukončil podobné experimenty. Loni se ovšem stal předsedou vlády Justin Trudeau, syn liberálního premiéra, který experiment v sedmdesátých letech pomohl financovat. Trudeau mladší podobným snahám rovněž fandí a je možné, že se dočkáme pokračování. Plány na nové testování základního příjmu se už ostatně vynořují v [Ontariu](http://www.huffingtonpost.ca/2016/02/26/ontario-basic-income_n_9328264.html) i [přímo v liberální vládě](http://www.theglobeandmail.com/news/politics/liberal-dominated-committee-calls-on-ottawa-to-study-guaranteed-income/article29184298/).


### Kde se základní příjem zkouší teď?

Dosud největší – a metodologicky nejrobustnější – experiment chystá Finsko. Do konce roku 2016 probíhá přípravná fáze, v tom příštím začne naostro a poběží dva roky. Základní příjem bude po celé zemi dostávat přibližně tisíc Finů a náklady na projekt budou 20 milionů eur. Design i provedení experimentu má na starost uskupení pěti univerzit s řadou externích expertů.

Za experimentem stojí finský premiér Juha Sipilä v čele pravostředové vlády. Po zvolení loni v květnu oznámil, že chce spustit řadu sociálních experimentů, mezi nimi dominuje právě základní příjem. Cílem experimentu je navrhnout reformu košatého finského sociálního systému tak, aby dokázal snížit byrokracii a motivoval k práci. Finové mají dobrý důvod: náklady na systém sociálního zabezpečení mají třetí nejvyšší v Evropě.

Pokus má vybrat z několika variant základního příjmu: ve hře je jednak plnohodnotný základní příjem (750 až 1000 eur), který by z velké části nahradil současný systém sociální správy, jednak částečný základní příjem (550 eur), který by současný systém pouze doplnil. Ve hře je také negativní daň z příjmu.

Experiment má probíhat na dvou úrovních. První část adresátů příjmu má být rovnoměrně rozprostřena napříč celým Finskem, vědecká metoda i finská ústava vyžaduje náhodný výběr. Kromě toho budou vybrány dvě lokality, kde příspěvek dostanou všichni. Účelem studie je totiž prozkoumat nejen chování jedinců, ale hlavně proměnu komunity pod vlivem základního příjmu. K tomu, kromě pokusu *„v terénu“*, mají sloužit také počítačové mikrosimulace.

Finská veřejnost experiment podporuje, pro je 69 procent země. V průzkumu z roku 2015 navrhli základní příjem na úrovni 1000 eur, tedy 1,3násobku minimální mzdy.

Výsledky pokusu budou k dispozici v roce 2019. Tentokrát jsou na ně zvědaví nejen vědci, ale i politici – je totiž možné, že ukážou cestu, jak některou formu základního příjmu uvést do praxe.

Kromě Finska stojí za to sledovat experimenty se základním příjmem v [Nizozemsku](http://qz.com/437088/utrecht-will-give-money-for-free-to-its-citizens-will-it-make-them-lazier/). Sociální programy v některých rozvojových zemích, které se snaží o odstranění chudoby, mají k univerzálnímu příjmu blízko: zajímavý je tři roky běžící program Recivitas v [Brazílii](http://www.basicincome.org/news/2012/06/brazil-basic-income-in-quatinga-velho-celebrates-3-years-of-operation/), vládní program pro mladé dospělé v [Ugandě](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2268552) a hlavně iniciativa GiveDirectly v [Keni](https://en.wikipedia.org/wiki/GiveDirectly), která letos připravuje mohutný experiment.
